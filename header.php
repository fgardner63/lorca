<?php

/* Lorca v0.0.1a  | Copyright (c) 2015 Frank Gardner (see LORCA-LICENSE for details) */

session_start();
require_once("rc4-drop1024.php");
require_once("sha256.php");
	
$load_html_page = $pagename;

if(isset($_SESSION['lorca_rand']) && !empty($_SESSION['lorca_rand']))
{
	mt_srand(crc32($_SESSION['lorca_rand']));
	$key = sha256(sha256($_SESSION['lorca_rand']).mt_rand());
}
else
{
	mt_srand(crc32(md5(microtime().mt_rand())));
	$key = sha256(sha256(session_id().mt_rand()));
}

if(file_exists($load_html_page))
{
	$data = @file_get_contents($load_html_page);
	if($data == '')
	{
		echo "File is empty";
		exit(0);
	}
}
else
{
	echo "File does not exist! ";
	exit(0);

}

$_SESSION['lorca_rand'] = sha256($key);
$encdata = bin2hex(crypt_rc4($key, $data));


?>


