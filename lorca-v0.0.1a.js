/* Lorca v0.0.1a  | Copyright (c) 2015 Frank Gardner (see LORCA-LICENSE for details) */
function hex2bin(r){for(var n=[],t=0;t<r.length-1;t+=2)n.push(parseInt(r.substr(t,2),16));return String.fromCharCode.apply(String,n)}
function crypt_rc4(r,o){for(var t,e=[],f=0,n="",a=0,c=0;256>c;c++)e[c]=c;for(a=1024;a>0;a--)for(c=0;256>c;c++)f=(f+e[c]+r.charCodeAt(c%r.length))%256,t=e[c],e[c]=e[f],e[f]=t;c=0,f=0;for(var h=0;h<o.length;h++)c=(c+1)%256,f=(f+e[c])%256,t=e[c],e[c]=e[f],e[f]=t,n+=String.fromCharCode(o.charCodeAt(h)^e[(e[c]+e[f])%256]);return n}
